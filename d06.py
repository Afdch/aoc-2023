from operator import mul
from functools import reduce
from math import sqrt, floor, ceil

with open('d06.txt') as f:
    lines = "".join(f).split('\n')

races1 = list(zip(*[[int(x) for x in line.split()[1:]] for line in lines]))
# print(races1)
races2 = [tuple([int("".join(line.split()[1:])) for line in lines])]
# print(races2)


# brutefoceish solution
# dumb algorith but if it works (in under a minute), I guess
def calculate(races):
    ways = []
    for T, D in races:
        w = 0
        # print(f'Race:')
        for t in range(T):
            if t*(T-t) > D:
                w += 1
        ways.append(w)
    return ways


# quadratic equation solution
# dumb implementation but if it works, I guess
def quadratic(races):
    ways = []
    for T, D in races:
        sqr = sqrt(T**2-4*D)
        t1 = floor((T - sqr)/2) + 1
        t2 = ceil((T + sqr)/2) - 1
        ways.append(t2 - t1 + 1)
    return ways


# bonus extra dumb solution:
# because people were not able to comprehend the power of bruteforce
def dumdum(races):
    ways = []
    for T, D in races:
        way = 0
        for ms in range(T):
            v = 0
            s = 0
            for t in range(ms):
                s += 0
                v += 1
            for t in range(T-ms):
                s += v

            if s > D:
                way += 1
        ways.append(way)
    return ways


# bruteforceish ~ 6.62 s
print("part 1:", reduce(mul, calculate(races1), 1))
print("part 2:", reduce(mul, calculate(races2), 1))


# quadratic ~ 0
print("part 1:", reduce(mul, quadratic(races1), 1))
print("part 2:", reduce(mul, quadratic(races2), 1))


# dumdum ~ heat death of the universe
print("part 1:", reduce(mul, dumdum(races1), 1))
print("part 2:", reduce(mul, dumdum(races2), 1))
