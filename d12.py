from functools import cache

lists = []

with open('d12.txt') as f:
    for line in f:
        springs, records = line.strip().split()
        records = tuple([int(x) for x in records.split(',')])
        lists.append((springs, records))


@cache
def smarter_brute(s: str, r, inf=''):
    if len(r) == 0:
        return 0 if len([x for x in s if x == '#']) > 0 else 1
    shift = 0
    intermediate = 0
    rec = r[0]
    # search = ''.join(["#" for _ in range(rec)])
    while True:
        # print(inf + s)
        # print(inf + ''.join([" " for _ in range(shift)]) + search)
        if (shift + rec > len(s) or
                shift + rec + len(r) - 1 + sum(r[1:]) > len(s) or
                (shift > 0 and s[shift - 1] == '#')):
            return intermediate
        if shift + rec == len(s):
            if s[shift:].replace('?', '#') == ''.join(["#" for _ in range(rec)]):
                return intermediate + 1
            elif len(r) == 1:
                return intermediate
            pass
        if s[shift + rec] == '#' or any([x == '.' for x in s[shift: shift+rec]]):
            shift += 1
            continue
        intermediate += smarter_brute(s[shift + rec + 1:], r[1:], ''.join(['_' for _ in range(shift + rec + 1)]) + inf)
        shift += 1


def solve(c=1):
    sm = 0
    for spring, record in lists:
        smbr = smarter_brute('?'.join([spring]*c), record*c)
        # print(smbr)
        sm += smbr
    print(sm)


# part 1
solve(1)
# part 2
# solve(5)
