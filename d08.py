# instructions for Nitro
# import https://www.youtube.com/watch?v=hz_gOFRwTcg
# cheers
from math import lcm

with open('d08.txt') as f:
    instructions, nodes = tuple("".join(f).split('\n\n'))
    node_ntw = {node[0:3]: (node[7:10], node[12:15]) for node in nodes.split('\n')}


# part 1
def part1(instructions: str):
    element = 'AAA'
    steps = 0
    while element != 'ZZZ':
        steps += 1
        instruction = instructions[0]
        instructions = instructions[1:] + instruction
        if instruction == 'L':
            element = node_ntw[element][0]
        else:
            element = node_ntw[element][1]
        # print(instruction, element)
    print(f'Part 1: {steps}')


def part2(instructions: str):
    print('Part 2:')
    els = [el for el in node_ntw if el[2] == 'A']
    print(els)
    steps = [0 for _ in els]
    reached = [False for _ in els]
    while not all(reached):
        instruction = instructions[0]
        instructions = instructions[1:] + instruction
        for i, el in enumerate(els):
            if el[2] != 'Z':
                steps[i] += 1
            else:
                reached[i] = True
            if not reached[i]:
                if instruction == 'L':
                    els[i] = node_ntw[els[i]][0]
                else:
                    els[i] = node_ntw[els[i]][1]
        # print(els) # debug
    print(steps)
    print(lcm(*steps))


part1(instructions)
part2(instructions)
