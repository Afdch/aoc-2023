with open('d01.txt') as f:
    lines = [line.strip() for line in f.readlines()]

# part 1
total = 0
for line in lines:
    line = [x for x in line if x.isdigit()]
    # print(line, line[0], line[-1])
    if len(line) > 0:   # so test input for part 2 wouldn't break
        total += int(line[0] + line[-1])
print(total)

# part 2
dig = {
    'one':      '1',
    'two':      '2',
    'three':    '3',
    'four':     '4',
    'five':     '5',
    'six':      '6',
    'seven':    '7',
    'eight':    '8',
    'nine':     '9'
}

# first attempt, didn't work
# total = 0
# for line in lines:
#     newline = ''
#     for character in line:
#         newline += character
#         for key in dig:
#             newline = newline.replace(key, dig[key])
#     newline = [x for x in newline if x.isdigit()]
#     print(line, newline, newline[0], newline[-1])
#     total += int(newline[0] + newline[-1])
# print(total)

# I am so so sorry
total = 0


def refactor(line, reversed=False) -> str:
    newline = ''
    for character in line[::-1] if reversed else line:
        if character.isdigit():
            return character
        newline += character
        if any([key in newline[::-1] if reversed else newline for key in dig]):
            for key in dig:
                if key in newline[::-1] if reversed else newline:
                    return dig[key]
    return newline


for line in lines:
    f = refactor(line)
    s = refactor(line, reversed=True)
    # print(line, f, s)

    total += int(f + s)

print(total)
