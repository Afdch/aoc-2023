from aoc import Maze
from collections import defaultdict
from printd import mkimg, savegif

with open('d14.txt') as f:
    M = Maze(f)


def cycle():
    # north
    for y in range(M.maxY + 1):
        for x in range(M.maxX + 1):
            if M.tiles[(x, y)] == 'O':
                y1 = y
                while y1 > 0 and M.tiles[(x, y1 - 1)] == '.':
                    M.tiles[(x, y1)] = '.'
                    y1 -= 1
                    M.tiles[(x, y1)] = 'O'
    # west
    for x in range(M.maxX + 1):
        for y in range(M.maxY + 1):
            if M.tiles[(x, y)] == 'O':
                x1 = x
                while x1 > 0 and M.tiles[(x1 - 1, y)] == '.':
                    M.tiles[(x1, y)] = '.'
                    x1 -= 1
                    M.tiles[(x1, y)] = 'O'
    # south
    for y in range(M.maxY, -1, -1):
        for x in range(M.maxX+1):
            if M.tiles[(x, y)] == 'O':
                y1 = y
                while y1 < M.maxY and M.tiles[(x, y1 + 1)] == '.':
                    M.tiles[(x, y1)] = '.'
                    y1 += 1
                    M.tiles[(x, y1)] = 'O'
    # east
    for x in range(M.maxX, -1, -1):
        for y in range(M.maxY + 1):
            if M.tiles[(x, y)] == 'O':
                x1 = x
                while x1 < M.maxX and M.tiles[(x1 + 1, y)] == '.':
                    M.tiles[(x1, y)] = '.'
                    x1 += 1
                    M.tiles[(x1, y)] = 'O'


def flatten(M: Maze) -> str:
    return ''.join([''.join([M.tiles[(x, y)] for x in range(M.maxX + 1)]) for y in range(M.maxY + 1)])


def calc_load(M):
    load = 0
    for rock in M.tiles:
        if M.tiles[rock] == 'O':
            ld = M.maxY - rock[1] + 1
            load += ld
    return load


# part 2:
cache = defaultdict(list)
loads = []
images = []
colours = {'.': 0, '#': 1, 'O': 4}
for i in range(1000000000):
    cycle()
    images.append(mkimg(M.tiles, 3, colourscheme=colours))
    cycle_start = 0
    loop = 0
    remained = 0
    loads.append(calc_load(M))
    fl = flatten(M)
    if fl in cache:
        if cycle_start == 0:
            cycle_start = i
            loop = i - cache[fl][0]
            x, remained = divmod(1000000000, loop)
        if i >= remained + loop:
            print(loop, remained, loads[remained + loop - 1])
            break
    cache[fl].append(i)
savegif(images, 'd14')
