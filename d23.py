import networkx as nx
from aoc import Maze, Tile
from functools import cache
import sys
import time

with open('d23.txt') as f:
    M = Maze(f, mode='maze')

print('import done')
source = (1, 0)
destination = (M.maxX-1, M.maxY)


def tile_to_complex(t: Tile): return t[0] + 1j*t[1]
def complex_to_tile(c: complex) -> Tile: return (int(c.real), int(c.imag))


@cache
def get_neighbours(tile: Tile):
    c = tile_to_complex(tile)
    nei: list[Tile] = []
    for d in [1, -1, 1j, -1j]:
        nc = c + d
        if not (0 <= nc.real <= M.maxX and 0 <= nc.imag <= M.maxY):
            continue
        nt = complex_to_tile(nc)
        if nt in M.walls:
            continue
        if (d, M.tiles[nt]) in [(1, '<'), (-1, '>'), (-1j, 'v'), (1j, '^')]:
            continue
        if (d, M.tiles[tile]) in [(1, '<'), (-1, '>'), (-1j, 'v'), (1j, '^')]:
            continue
        nei.append(nt)
    return nei


def findPath(source: Tile, destination: Tile, path=tuple()):
    global max_path
    # print(f'Part 1: {max_path}        ', end='\r')
    for tile in get_neighbours(source):
        if tile == destination:
            max_path = max(max_path, len(path) + 1)
        if tile in path:
            continue
        findPath(tile, destination, (*path, source))


max_path = 0
t = time.perf_counter()
# maze size ~140x140, with ~1/2 being walls140*140 -> 9800 ish steps
sys.setrecursionlimit(15000)
findPath(source, destination)
print(f'Part 1: {max_path}, done in {time.perf_counter() - t:6.4f}')

d = {source: 'S', destination: 'E'}
M.tiles.update(d)
M.objects.update(d)

M.objects = d
M.build_graph()
M.show_graph()
max_path = 0
t = time.perf_counter()
for i, path in enumerate(nx.all_simple_paths(M.graph, source, destination)):
    dist = 0
    for i in range(len(path)-1):
        dist += M.graph[path[i]][path[i+1]]['weight']
    max_path = max(max_path, dist)
print(f'Part 2: {max_path}, done in {time.perf_counter() - t:6.4f}')
