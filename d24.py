import numpy as np
from itertools import combinations

type ray = tuple[np.ndarray, np.ndarray]
with open('d24.txt') as f:
    rays: list[ray] = [tuple(np.array([int(c) for c in ln.split(', ')], dtype=np.longdouble)
                             for ln in line.strip().split(' @ '))
                       for line in f]  # type: ignore


# reference: Intersection of two lines in three-space,
#           Ronald Goldman, Graphics Gems, page 304, 1990)
# via https://songho.ca/math/line/line.html
def rays_intersect_2dp(ray1: ray, ray2: ray):
    p = ray1[0]
    v = ray1[1]
    q = ray2[0]
    u = ray2[1]
    a = np.cross(v, u)
    dot = np.dot(a, a)
    if (dot == 0):
        return None
    b = np.cross((q - p), u)
    t = np.dot(b, a) / dot
    if t < 0:
        return None
    return p + (t * v)


def rays_intersect_2d(r1, r2):
    # I am too lazy to do math, so do it 2 times instead
    p1 = rays_intersect_2dp(r1, r2)
    if p1 is None:
        return None
    p2 = rays_intersect_2dp(r2, r1)
    if p2 is None:
        return None
    if (p1 != p2).any():
        raise Exception
    return p1


def within_confines(p, c_min, c_max): return all(c_min <= int(c) <= c_max for c in p)
def xy_proj(p3d: ray) -> ray: return tuple(np.array([p[0], p[1]]) for p in p3d)  # type: ignore
def pos_time(r, p): return (p - r[0])/r[1] >= 0


inters = 0
for ray1, ray2 in combinations(rays, 2):
    intersection = rays_intersect_2d(xy_proj(ray1), xy_proj(ray2))
    if intersection is not None and \
            within_confines(intersection, 200000000000000, 400000000000000):
        inters += 1
print(inters)
