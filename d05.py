from collections import defaultdict

with open('d05.txt') as f:
    lines = "".join(f).split('\n\n')

# print(lines)
seeds = [int(x) for x in lines[0].split()[1:]]
# print(seeds)

maps = defaultdict(list)
chains = {}
for entry in lines[1:]:
    entry = entry.split('\n')
    source, _, destination = entry[0].split(' ')[0].split('-')
    # print(source, destination)
    chains[source] = destination
    for category in entry[1:]:
        sourceNo, destinationNo, rangeL = [int(x) for x in category.split()]
        # print(sourceNo, destinationNo, rangeL)
        maps[source].append((destinationNo, sourceNo, rangeL))

locations = []


def search_category(number, map):
    for destinationNo, sourceNo, rangeL in maps[map]:
        if destinationNo <= number < destinationNo + rangeL:
            new_number = - destinationNo + number + sourceNo
            # print(chains[map], new_number)
            return new_number
    # print(chains[map], number)
    return number


def location_from_seed(seed):
    # print(f"\nSeed {seed}")
    soil = search_category(seed, 'seed')
    fertilizer = search_category(soil, 'soil')
    water = search_category(fertilizer, 'fertilizer')
    light = search_category(water, 'water')
    temperature = search_category(light, 'light')
    humidity = search_category(temperature, 'temperature')
    location = search_category(humidity, 'humidity')
    locations.append(location)


def part1():
    for seed in seeds:
        location_from_seed(seed)


# presort
# [maps[map].sort(key=lambda x: x[1]) for map in maps]
[maps[map].sort() for map in maps]
# print(maps)
min_value = None


def set_min_value(value):
    global min_value
    if min_value is None:
        min_value = value
    else:
        min_value = min(value, min_value)


scans = 0


def next_level(A, Ax, map='seed'):
    global scans
    scans += 1
    if Ax == 0:
        return
    # print(map, A)
    if map == 'location':
        set_min_value(A)
        return
    Bs, srcs, y = zip(*maps[map])

    for d in range(len(Bs)):
        shift = srcs[d] - Bs[d]
        By = Bs[d] + y[d]
        B = Bs[d]
        if Ax < B:                                                      # 1
            next_level(A, Ax, chains[map])
            return
        elif Ax <= By:                                                  # 2 & 4
            if A < B:                                                   # 2
                # section 1:
                next_level(A, B - 1, chains[map])
                next_level(B+shift, Ax + shift, chains[map])
                # section 2:
            else:                                                       # 4
                next_level(A + shift, Ax + shift, chains[map])
                return
        else:                                                           # 3 & 5
            if A < B:                                                   # 3
                next_level(A, B - 1, chains[map])
                next_level(B + shift, By + shift - 1, chains[map])
                next_level(By, Ax, map)
                return
            elif A < By:                                                # 5
                # section 1:
                next_level(A + shift, By + shift - 1, chains[map])
                next_level(By, Ax, map)
                return
                # section 2:

    if A > Bs[-1] + y[-1]:
        next_level(A, Ax, chains[map])
    else:
        pass


def part2():
    # I can' be bothered refactoring and combing the code
    starts = seeds[::2]
    lengths = seeds[1::2]
    for i, start in enumerate(starts):
        next_level(start, start + lengths[i])


part2()
# print(min(locations))
print(f'part 2: {min_value}')
print(scans)
