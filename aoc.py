import queue
from collections import defaultdict
import printd

Tile = tuple[int, int]


class Maze:
    """
    An AoC standard Maze class used to store and find a path within the maze


    Parameters
    ----------

    file : TextIOWrapper

    mode : string(default=None)
        Mode {'maze', 'field'}

    walltile: string(default='#')
        If mode = 'maze', walltile specifies what character counts as a wall

    emptytile: string(default=',')
        if mode is 'maze' or 'field', specifies what character couns as empty

    Attributes
    ----------
    tiles: dict
        dictionary keys in (x, y) format; value is a character displayed

    walls: list
        a list of wall tiles in the maze

    objects: list
        a list of coordinates of non-wall non-empty tiles in the maze


    """
    def __init__(self, file, mode=None, walltile="#", emptytile=".") -> None:
        self.tiles, self.features = self.__readmaze(file)
        self.maxX: int = max([x for x, _ in self.tiles.keys()])
        self.maxY: int = max([y for _, y in self.tiles.keys()])
        self.mode = mode

        if self.mode == 'maze':
            self.walls: list[Tile] = [x for x in self.tiles.keys() if self.tiles[x] == walltile]
            self.objects: dict = {x: self.tiles[x] for x in self.tiles if self.tiles[x] not in [emptytile, walltile]}
        elif self.mode == 'field':
            self.objects: dict = {x: self.tiles[x] for x in self.tiles if self.tiles[x] not in [emptytile]}
        elif self.mode == 'numeric':
            self.tiles_int = {t: int(self.tiles[t]) for t in self.tiles}

        self.neighbours: dict[Tile, list[Tile]] = {x: self.get_neighbours(x) for x in self.tiles}

    # region boringmethods
    def __readmaze(self, f) -> tuple[dict[Tile, str], dict[str, list[Tile]]]:
        """Specify an AoC standard maze file read with open()"""
        fullmaze = {}
        features = defaultdict(list)
        for j, line in enumerate(f):
            for i, char in enumerate(line.strip()):
                fullmaze[(i, j)] = char
                features[char].append((i, j))
        return fullmaze, features

    def isWall(self, tile: Tile) -> bool:
        return tile in self.walls

    def isEmptyorObject(self, tile: Tile) -> bool:
        return tile not in self.walls
    # endregion

    # region fancymethods
    def flatten(self) -> str:
        """
            Return a string representation of a Maze
        """
        return '\n'.join([''.join([str(self.tiles[(x, y)]) for x
                                   in range(self.maxX + 1)])
                          for y in range(self.maxY + 1)])

    def get_neighbours(self, tile: Tile, mode='cardinal'):
        """Return a list of neighbours of a tile in the maze

        Parameters
        ----------
        tile : tuple(int, int)
            Tile for which the neighbours are searched
        mode : string(default='cardinal')
            Mode: {'cardinal', 'full', 'diagonal'}
        """
        x, y = tile
        nei: list[Tile] = []
        match mode:
            case 'full':
                neighbours = [(x + 1, y + 1), (x + 1, y), (x + 1, y - 1),
                              (x, y + 1), (x, y - 1),
                              (x - 1, y + 1), (x - 1, y), (x - 1, y - 1)]
            case 'diagonal':
                neighbours = [(x + 1, y + 1), (x + 1, y - 1),
                              (x - 1, y + 1), (x - 1, y - 1)]
            case 'cardinal' | _:
                neighbours = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]
        for x1, y1 in neighbours:
            if 0 <= x1 <= self.maxX and 0 <= y1 <= self.maxY:
                if not (self.mode == 'maze' and (x1, y1) in self.walls):
                    nei.append((x1, y1))
        return nei

    def __heuristic(self, a: Tile, b: Tile) -> int:
        return abs(a[0] - b[0]) + abs(a[1] - b[1])

    def reduce_maze(self):
        deadends = queue.Queue()
        for tile in self.tiles:
            if tile not in self.walls:
                neis = self.get_neighbours(tile)
                if len(neis) == 1:
                    deadends.put(tile)

        while not deadends.empty():
            tile = deadends.get()
            if tile not in self.objects:
                n = self.get_neighbours(tile)
                if len(n) == 1:
                    deadends.put(n[0])
                    self.tiles[tile] = '#'  # type: ignore
                    self.walls.append(tile)
        else:
            print('done')

    def findPath(self, source: Tile, destination: Tile, mode: str = 'distance'):
        """A* path from source to destination

        Parameters
        ----------
        source : tuple(int, int)

        destination : tuple(int, int)

        mode : string(default='distance')
            Mode: {'distance', 'path'}

        Returns
        -------
        in 'distance' mode: an integer with a cost to move from source to destination
        in 'path' mode: a list of tuples with coordinates
        """
        # TODO cost as input?
        from queue import PriorityQueue
        cost = {source: 0}
        frontier: PriorityQueue[tuple[int, Tile]] = PriorityQueue()
        path = {}
        for tile in self.neighbours[source]:
            frontier.put((self.__heuristic(tile, destination)+1, tile))
            cost[tile] = int(self.tiles[tile]) if self.mode == 'numeric' else 1
            path[tile] = source

        while not frontier.empty():
            _, current = frontier.get()

            if current == destination and _ <= cost[destination]:
                match mode:
                    # TODO possible modes:
                    # distance -> int value (default)
                    case 'distance':
                        return cost[current]
                    case 'path':
                        c = destination
                        p = [c]
                        while c != source:
                            c = path[c]
                            p.append(c)
                        return list(reversed(p))

            for next in self.neighbours[current]:
                next_cost = cost[current] + int(self.tiles[next]) if self.mode == 'numeric' else 1
                if next not in cost or next_cost < cost[next]:
                    cost[next] = next_cost
                    path[next] = current
                    priority = next_cost + self.__heuristic(next, destination)
                    frontier.put((priority, next))
    # endregion

    # region graphs

    def build_graph(self, show=False):
        """
        Builds a full graph of the maze.
        vertices are any intersections and objects within the maze
        edges meausre distance between the vertices

        networkx compatible
        """
        import networkx as nx
        from queue import PriorityQueue
        self.graph = nx.Graph()
        # 1. select a random point from objects

        if self.objects:
            start = list(self.objects.keys())[0]
        else:
            return None

        self.graph.add_node(start)
        frontier: PriorityQueue[tuple[int, Tile, Tile, Tile]] = PriorityQueue()
        frontier.put((0, start, start, start))
        visited: set[Tile] = {start}

        while not frontier.empty():
            cost, last_node, last_t, curr_t = frontier.get()
            neighs = self.get_neighbours(curr_t)
            if ((len(neighs) > 2 or
                 curr_t in self.objects or
                 curr_t in self.graph)
                    and (last_node != curr_t or cost > 1)):
                self.graph.add_edge(last_node, curr_t, weight=cost)
                cost = 0
                last_node = curr_t
                visited |= {last_t}
            for next_t in neighs:
                if next_t == last_t:
                    continue
                if ((next_t not in visited) or next_t in self.graph.nodes) and next_t != last_node:
                    # frontier.put((last_node, next, cost + 1))
                    frontier.put((cost + 1, last_node, curr_t, next_t))

        if False:
            # TODO graph reduction logic
            self.graph_reduce()

        if show:
            self.show_graph()
        return self.graph

    def graph_reduce(self):
        import networkx as nx
        delete_nodes = []
        make_neighbours = []
        for node in nx.nodes(self.graph):
            if node not in self.objects:
                neighbours = list(nx.neighbors(self.graph, node))
                if len(neighbours) == 2:
                    delete_nodes.append(node)
                    make_neighbours.append(neighbours)
        else:
            for neighbours in make_neighbours:
                self.graph.add_edge(neighbours[0], neighbours[1])
            for node in delete_nodes:
                self.graph.remove_node(node)

    def show_graph(self, node_pos=None, node_labels=None, node_colors=None):
        # TODO: stylize
        # from matplotlib import cm
        import matplotlib.pyplot as plt
        import networkx as nx

        node_pos = {x: x for x in self.tiles} if node_pos is None else node_pos
        node_labels = {x: self.tiles[x] if x in self.objects else '' for x in self.tiles}
        edge_labels = nx.get_edge_attributes(self.graph, 'weight')

        nx.draw(
            self.graph,
            pos=node_pos,
            with_labels=True,
            node_size=200,
            # node_color = '#530aab',
            # cmap = 'paired',
            labels=node_labels,
            font_size=10,
            font_weight='bold',
            font_color='#aaa'
        )

        nx.draw_networkx_edge_labels(
            self.graph,
            pos=node_pos,
            edge_labels=edge_labels
        )

        plt.gca().invert_yaxis()
        plt.show()
    # endregion

    # region printing
    def print(self, tileset=None, reverse=False):
        printd.printd(self.tiles, tileset=tileset, reverse=reverse)
        pass

    def __str__(self):
        self.print()
        return ('')
    # endregion


def print_percent(num, den, title='', scale=True, value=None):
    percent = 100 * (num + 1) / den

    if title != '':
        # title = ('\033[1A' if num > 0 else '') + title + '\n'  # '\t'
        title = title + '\n'  # '\t'

    if percent < 40:
        prefix = '\033[91m'
    elif percent < 80:
        prefix = '\033[93m'
    elif percent < 100:
        prefix = '\033[94m'
    else:
        percent = 100
        prefix = '\033[92m'

    sc = '\t[' + '█'*int(percent/5) + '░'*(20 - int(percent / 5)) + ']\t' if scale else ''
    postfix = '\033[94m' + str(value) if value else ''
    end = "\n" if percent >= 100 else "\r" if title == '' else '\033[1A\r'

    print(f'{title}{prefix}{percent:6.2f}%{sc} {postfix:{len(postfix) + 5}}\033[0m', end=end)
