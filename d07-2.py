from collections import Counter
with open('d07.txt') as f:
    lines: list[tuple[str, int]] = [(line[0].replace('A', 'F').replace('K', 'D').
                                     replace('Q', 'C').replace('J', 'B').replace('T', 'A'),
                                     int(line[1])) for line in [line.strip().split() for line in f.readlines()]]


def srt1(card: str) -> int:
    cnt = Counter(card)
    j = cnt['1']
    mc = cnt.most_common(5)
    if j == 5:
        return 7
    elif j > 0:
        del cnt['1']
        cnt[cnt.most_common(1)[0][0]] += j
        mc = cnt.most_common(5)
    c = {x[1] for x in mc}
    if c == {5}:
        return 7
    if c == {4, 1}:
        return 6
    if c == {3, 2}:
        return 5
    if c == {3, 1}:
        return 4
    if c == {2, 1}:
        if len(mc) == 3:
            return 3
        else:
            return 2
    if c == {1}:
        return 1
    return 0


# part 1
m = 0
for i, line in enumerate(sorted(lines, key=lambda x: (srt1(x[0]), x[0][0], x[0][1], x[0][2], x[0][3], x[0][4]))):
    # print(f'{i+1:>4}, {line}, {srt1(line[0])}')
    m += (i+1)*line[1]
print(m)

# part 2
lines = [(x.replace('B', '1'), y) for (x, y) in lines]
m = 0
for i, line in enumerate(sorted(lines, key=lambda x: (srt1(x[0]), x[0][0], x[0][1], x[0][2], x[0][3], x[0][4]))):
    # print(i+1,line)
    m += (i+1)*line[1]
print(m)
