from aoc import Maze
from queue import Queue
from printd import printd, mkimg
from PIL import ImageDraw
Tile = tuple[int, int]


with open('d10.txt') as f:
    M = Maze(f)

replacement = {
    '|': [' ', '│', ' ',
          ' ', '│', ' ',
          ' ', '│', ' '],
    '-': [' ', ' ', ' ',
          '─', '─', '─',
          ' ', ' ', ' '],
    'J': [' ', '│', ' ',
          '─', '┘', ' ',
          ' ', ' ', ' '],
    'L': [' ', '│', ' ',
          ' ', '└', '─',
          ' ', ' ', ' '],
    'F': [' ', ' ', ' ',
          ' ', '┌', '─',
          ' ', '│', ' '],
    '7': [' ', ' ', ' ',
          '─', '┐', ' ',
          ' ', '│', ' '],
    '.': [' ', ' ', ' ',
          ' ', '?', ' ',
          ' ', ' ', ' '],
    'S': ['┌', '┴', '┐',
          '┤', '!', '├',
          '└', '┬', '┘'],
}

start = M.features['S'][0]
pipes = ["-", '|', 'J', 'F', 'L', '7']
print(start)
Q = Queue()

for n in [(1, 0), (-1, 0), (0, -1), (0, 1)]:
    x, y = n
    nei = start[0] + x, start[1] + y
    if nei[0] < 0 or nei[0] > M.maxX or nei[1] < 0 or nei[1] > M.maxY:
        continue
    test = M.tiles[nei]
    if (((x == 1) and test not in ['-', 'J', '7']) or
        ((x == -1) and test not in ['-', 'F', 'L']) or
        ((y == 1) and test not in ['|', 'J', 'L']) or
        ((y == -1) and test not in ['|', 'F', '7'])):
        continue
    Q.put((nei, 1))


explored = [start]
dists = {start[0]: 0}


def get_neighbours(tile: Tile):
    x, y = tile
    nei: list[Tile] = []
    form = M.tiles[tile]
    if form == '-':
        neighbours = [(x + 1, y), (x - 1, y)]
    elif form == '|':
        neighbours = [(x, y + 1), (x, y - 1)]
    elif form == 'J':
        neighbours = [(x, y - 1), (x - 1, y)]
    elif form == 'F':
        neighbours = [(x + 1, y), (x, y + 1)]
    elif form == 'L':
        neighbours = [(x, y - 1), (x + 1, y)]
    elif form == '7':
        neighbours = [(x - 1, y), (x, y + 1)]
    else:
        neighbours = []
    for x1, y1 in neighbours:
        if 0 <= x1 <= M.maxX and 0 <= y1 <= M.maxY:
            if (x1, y1) not in explored:
                nei.append((x1, y1))
    return nei


def part1():
    while Q.qsize() > 0:
        current, dist = Q.get()
        dists[current] = dist
        explored.append(current)
        for nei in get_neighbours(current):
            if nei in explored:
                continue
            if M.tiles[nei] not in pipes:
                continue
            Q.put((nei, dist + 1))
    # print(dists.values())


part1()
print(max(dists.values()))


# part 2
printd(M.tiles, reverse=False)

aMAZEing = {}
for tile in M.tiles:
    if tile in explored:
        shape = replacement[M.tiles[tile]]
    else:
        shape = replacement['.']
    for n in range(3):
        for m in range(3):
            a_tile_x = tile[0] * 3 + m
            a_tile_y = tile[1] * 3 + n
            aMAZEing[(a_tile_x, a_tile_y)] = shape[m + 3*n]

printd(aMAZEing, reverse=False)
colourscheme = {
    '─': 1, '│': 1, '!': 1,
    '┌': 1, '┐': 1, '┘': 1, '└': 1,
    '┤': 1, '┴': 1, '├': 1, '┬': 1,
    '?': 2, ' ': 0
}
colourpalette = [
      0,      0,      0,    # 0 #000
    255,    255,    255,    # 1 #fff
    255,      0,      0,    # 2 #f00

]

image = mkimg(aMAZEing, 1, reverse=False, colourscheme=colourscheme, colourpalette=colourpalette)
image = image.convert('RGB')
image.save('d10-0.png', bitmap_format='png')
# image.show()
ImageDraw.floodfill(image, (0, 0), (255, 0, 0))      # #f00
image.save('d10-1.png', bitmap_format='png')
ImageDraw.floodfill(image, (0, 0), (255, 255, 255))  # #fff
image.save('d10-2.png', bitmap_format='png')
ImageDraw.floodfill(image, (0, 0), (0, 0, 0))        # #000
image.save('d10-3.png', bitmap_format='png')
ImageDraw.floodfill(image, (0, 0), (255, 255, 255))  # #fff
image.save('d10-3.png', bitmap_format='png')
# image.show()

# input()
cnt = 0  # 1 # why did I initialize it as 1??
# image = Image.open('out.png')
for pixel in image.getdata():
    # print(pixel)
    if pixel == (255, 0, 0):
        cnt += 1
print(cnt)

# 452 is too high
