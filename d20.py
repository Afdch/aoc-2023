import networkx as nx
import matplotlib.pyplot as plt

# pulse propagator
PP = nx.DiGraph()

with open('d20.txt') as f:
    for line in f:
        typ = ''
        source, targets = line.strip().split(' -> ')
        if source[0] == '%':
            source, typ = source[1:], source[0]
            PP.add_node(source, typ=typ, state='OFF')
        elif source[0] == '&':
            source, typ = source[1:], source[0]
            PP.add_node(source, typ=typ)
        else:
            PP.add_node(source, typ=None)
        for target in targets.split(', '):
            PP.add_edge(source, target)


def draw():
    kkl = nx.kamada_kawai_layout(PP)
    nx.draw(PP, pos=kkl, with_labels=True)
    # nx.draw_networkx_nodes(PP, kkl, {n: n+PP.nodes[n]['typ'] for n in PP})
    plt.draw()
    plt.show()


for n in PP:
    if 'typ' not in PP.nodes[n]:
        PP.nodes[n]['typ'] = None
    if PP.nodes[n]['typ'] == '&':
        PP.nodes[n]['inputs'] = {m: 'LOW' for m in PP.pred[n]}

LOW_CNT, HIGH_CNT = 0, 0
cycle = 0
cycles = {}
targets = PP.pred[list(PP.pred['rx'])[0]]


def button_press(part1):
    global cycle, LOW_CNT, HIGH_CNT
    cycle += 1
    callstack: list[tuple] = [('broadcaster', x, 'LOW') for x in PP['broadcaster']]
    LOW_CNT += 1 + len(callstack)

    while callstack:
        fr, to, level = callstack.pop(0)
        match PP.nodes[to]['typ']:
            case '%':
                # % Flip-flop
                # % ignores HIGH
                # % when [receives LOW], FLIPS
                # % when [FLIPS], if was OFF, OFF -> ON, sends HIGH
                # % when [FLIPS], if was ON, ON -> OFF, sends LOW
                if level == 'HIGH':
                    continue
                if PP.nodes[to]['state'] == 'OFF':
                    PP.nodes[to]['state'] = 'ON'
                    for nxt in PP[to]:
                        callstack.append((to, nxt, 'HIGH'))
                        HIGH_CNT += 1
                else:
                    PP.nodes[to]['state'] = 'OFF'
                    for nxt in PP[to]:
                        callstack.append((to, nxt, 'LOW'))
                        LOW_CNT += 1
            case '&':
                # & Conjunction
                # & remembers all recent incoming pulses
                # & default all imputs in LOW
                # & imput pulse RECEIVED -> STORED
                # & if STORED ALL HIGH, sends LOW
                # & if STORED NOT ALL HIGH, sends HIGH
                PP.nodes[to]['inputs'][fr] = level
                if all([PP.nodes[to]['inputs'][i] == 'HIGH' for i in PP.nodes[to]['inputs']]):
                    for nxt in PP[to]:
                        callstack.append((to, nxt, 'LOW'))
                        LOW_CNT += 1
                else:
                    for nxt in PP[to]:
                        callstack.append((to, nxt, 'HIGH'))
                        HIGH_CNT += 1
            case _:
                # raise Exception
                # print('yay')
                pass
        if not part1:
            for item in callstack:
                # TOO SLOW
                # if item[1] == 'rx' and item[2] == 'LOW':
                #     print(cycle)
                #     break
                if item[1] in targets and item[2] == 'LOW':
                    if item[1] not in cycles:
                        cycles[item[1]] = cycle
                    if all([x in cycles for x in targets]):
                        import math
                        print(math.lcm(*cycles.values()))
                        return True

    if part1 and cycle == 1000:
        print(LOW_CNT, HIGH_CNT, LOW_CNT * HIGH_CNT)
        # break  # for part 1
    if cycle % 10000 == 0:
        print(f'{cycle:15}', end='\r')


# part 1
# for i in range(1000):
#     button_press(True)
# part 2
while button_press(False) is not True:
    pass
