def hash1(string: str):
    cv = 0
    for character in string:
        cv += ord(character)
        cv = (cv * 17) % 256
    return cv


box = [{} for x in range(256)]
S = 0
with open('d15.txt') as f:
    # M = Maze(f)
    for line in f:
        for substr in line.strip().split(','):
            if '-' in substr:
                label = substr[:-1]
                bx = hash1(label)
                if label in box[bx]:
                    del (box[bx][label])
            else:
                label = substr[:-2]
                bx = hash1(label)
                focal = substr[-1]
                box[bx][label] = focal

            # part 1
            # print(substr)
            S += (hash1(substr))
print(S)

# part 2
S2 = 0
for i, bz in enumerate(box):
    for j, item in enumerate(bz):
        x = (i + 1) * (j + 1) * int(bz[item])
        S2 += x
print(S2)
