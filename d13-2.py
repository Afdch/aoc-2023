import numpy as np

with open('d13.txt') as f:
    mirrors: list[str] = f.read().split('\n\n')


def find_mirror(a, transposed=False, reject=0):
    m = np.where((a == a[0]).all(axis=1))[0]
    if len(m) >= 2:
        for tt in m[1:]:
            if tt % 2 == 0:
                continue
            i = int((tt + 1) / 2)
            if (a[:i] == a[i:tt+1][::-1]).all():
                if i != reject:
                    return i
    m = np.where((a == a[-1]).all(axis=1))[0]
    if len(m) >= 2:
        for tt in m[:-1]:
            if (m[-1] - tt) % 2 == 0:
                continue
            i = int(m[-1] - (m[-1] - tt + 1) / 2) + 1
            if (a[tt:i] == a[i:][::-1]).all():
                if i != reject:
                    return i
    if not transposed:
        return 100 * find_mirror(np.transpose(a), True, int(reject/100))
    return 0


def repair_mirror(a: np.ndarray):
    broken = find_mirror(a)
    for y in range(len(a)):
        for x in range(len(a[0])):
            a[y][x] = 0 if a[y][x] == 1 else 1
            maybe = find_mirror(a, reject=broken)
            if maybe > 0:
                return maybe
            a[y][x] = 0 if a[y][x] == 1 else 1
    raise Exception


S = 0
for mirror in mirrors:
    a = np.array([[1 if x == '#' else 0 for x in line] for line in mirror.split()])
    X = repair_mirror(np.transpose(a))
    print(X)
    S += X
print(S)
