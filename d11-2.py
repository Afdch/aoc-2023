from aoc import Maze
from itertools import combinations

with open('d11.txt') as f:
    M = Maze(f, mode='field', emptytile='.')

emptycols = []
emptyrows = []

for x in range(M.maxX+1):
    if all([M.tiles[(x, y)] == '.' for y in range(M.maxY+1)]):
        emptycols.append(x)
for y in range(M.maxY+1):
    if all([M.tiles[(x, y)] == '.' for x in range(M.maxX+1)]):
        emptyrows.append(y)

# print(M.objects)
# print(emptyrows, emptycols)


def heuristic2(A, B, factor=2):
    x1, y1 = A
    x2, y2 = B
    if x1 > x2:
        x1, x2 = x2, x1
    if y1 > y2:
        y1, y2 = y2, y1
    x3 = len([x for x in emptycols if x1 < x and x < x2])
    y3 = len([y for y in emptyrows if y1 < y and y < y2])

    return y2 - y1 + x2 - x1 + (x3 + y3)*(factor - 1)


def sumuniverse(factor=2):
    return sum([heuristic2(A, B, factor) for A, B in combinations(M.objects, 2)])


# part 1:
print(sumuniverse())

# part 2:
print(sumuniverse(1000000))
