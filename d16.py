from aoc import Maze, Tile
from queue import Queue
from itertools import product
from functools import cache
# from printd import printd

with open('d16.txt') as f:
    M = Maze(f, mode='maze')

# print(M.flatten())
# print(M.objects)


def move(pos, dir):
    x, y = pos
    match dir:
        case 1:
            if x < M.maxX:
                return x + 1, y
        case -1:
            if x > 0:
                return x - 1, y
        case 1j:
            if y < M.maxY:
                return x, y + 1
        case -1j:
            if y > 0:
                return x, y - 1
    return None


def beam_traverse(start_pos, dir: complex):
    beams: Queue[tuple[Tile, complex]] = Queue()
    beams.put((start_pos, dir))

    energized = []

    while not beams.empty():
        pos, dir = beams.get()
        if (pos, dir) in energized:
            continue
        energized.append((pos, dir))
        next_pos = move(pos, dir)
        if next_pos is None:
            continue
        if next_pos not in M.objects:
            beams.put((next_pos, dir))
        match M.tiles[next_pos], dir:
            case ('|', 1) | ('|', -1):
                beams.put((next_pos, 1j))
                beams.put((next_pos, -1j))
            case ('-', 1j) | ('-', -1j):
                beams.put((next_pos, 1))
                beams.put((next_pos, -1))
            case ('/', 1) | ('\\', -1):
                beams.put((next_pos, -1j))
            case ('\\', 1) | ('/', -1):
                beams.put((next_pos, 1j))
            case ('/', 1j) | ('\\', -1j):
                beams.put((next_pos, -1))
            case ('/', -1j) | ('\\', 1j):
                beams.put((next_pos, 1))
            case _:
                beams.put((next_pos, dir))
    E = {x[0] for x in energized}
    return len(E) - 1   # to account (-1, 0) virtual tile


# part 1:
# print(beam_traverse((-1, 0), 1))


# # part 2 nobrainer:
# all_paths = []
# for x in range(M.maxX + 1):
#     all_paths.append(beam_traverse((x, -1), 1j))
#     all_paths.append(beam_traverse((x, M.maxY + 1), -1j))
#     print(f'{x / (M.maxX + 1) * 100:6.2f}%', end='\r')
# for y in range(M.maxY + 1):
#     all_paths.append(beam_traverse((-1, y), 1))
#     all_paths.append(beam_traverse((M.maxX + 1, y), -1))
#     print(f'{y / (M.maxY + 1) * 100:6.2f}%', end='\r')
# print('\n', max(all_paths))


# a slightly better solution

pointers: dict[tuple[Tile, complex], tuple[list[tuple[Tile, complex]], set[Tile]]] = {}

points = list(product(M.objects, [1, 1j, -1, -1j]))
edges = [((x, -1), 1j) for x in range(M.maxX + 1)] + \
    [((x, M.maxY + 1), -1j) for x in range(M.maxX + 1)] + \
    [((-1, y), 1) for y in range(M.maxY + 1)] + \
    [((M.maxX + 1, y), -1) for y in range(M.maxY + 1)]

for object, dir in points + edges:
    pos = object
    tiles = set()
    next_posdir = []
    while (next_pos := move(pos, dir)) is not None:
        pos = next_pos
        tiles |= {pos}
        if next_pos in M.objects:
            # TODO check for object collision
            match M.tiles[next_pos], dir:
                case ('|', 1) | ('|', -1):
                    next_posdir.append((next_pos, 1j))
                    next_posdir.append((next_pos, -1j))
                case ('-', 1j) | ('-', -1j):
                    next_posdir.append((next_pos, 1))
                    next_posdir.append((next_pos, -1))
                case ('/', 1) | ('\\', -1):
                    next_posdir.append((next_pos, -1j))
                case ('\\', 1) | ('/', -1):
                    next_posdir.append((next_pos, 1j))
                case ('/', 1j) | ('\\', -1j):
                    next_posdir.append((next_pos, -1))
                case ('/', -1j) | ('\\', 1j):
                    next_posdir.append((next_pos, 1))
                case _:
                    continue
            break
    else:
        # next_posdir.append((next_pos, 1))
        pass
        
    # TODO add list to pointers
    pointers[(object, dir)] = (next_posdir, tiles)


@cache
def traverse(posdir):
    visited = []
    tr_tiles = set()
    Q = Queue()
    Q.put(posdir)
    while not Q.empty():
        pos, dir = Q.get()
        if (pos, dir) in visited:
            continue
        visited.append((pos, dir))
        next_posdirs, path = pointers[(pos, dir)]
        tr_tiles |= path
        for next_posdir in next_posdirs:
            if next_posdir not in visited:
                Q.put(next_posdir)
    return len(tr_tiles)


# part 1
print(traverse(((-1, 0), 1)))
# part 2 small brainer
all_paths = []
for x in range(M.maxX + 1):
    all_paths.append(traverse(((x, -1), 1j)))
    all_paths.append(traverse(((x, M.maxY + 1), -1j)))
    print(f'{x / (M.maxX + 1) * 100:6.2f}%', end='\r')
for y in range(M.maxY + 1):
    all_paths.append(traverse(((-1, y), 1)))
    all_paths.append(traverse(((M.maxX + 1, y), -1)))
    print(f'{y / (M.maxY + 1) * 100:6.2f}%', end='\r')
print('\n', max(all_paths))
