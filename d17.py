from aoc import Maze, Tile
type Dir = tuple[int, int]
cartesian = (1, 1j, -1, -1j)
with open('d17.txt') as f:
    M = Maze(f)
# print(M.flatten())


def heuristic(a: Tile, b: Tile) -> int: return abs(a[0] - b[0]) + abs(a[1] - b[1])
def tile_to_complex(t: Tile): return t[0] + 1j*t[1]
def complex_to_tile(c: complex) -> Tile: return (int(c.real), int(c.imag))


def branch(t: Tile, d: complex) -> list[tuple[Tile, complex, int]]:
    global rn
    rlst = []
    for r in rn:
        ttiles = [complex_to_tile(tile_to_complex(t) + d*x) for x in range(1, r)]
        if all([t in M.tiles for t in ttiles]):
            for dir in [d*1j, d/1j]:
                rlst.append((ttiles[-1], dir, sum([int(M.tiles[t]) for t in ttiles])))
    return rlst


def findPath(source: Tile, destination: Tile, mode: str = 'distance'):

    from queue import PriorityQueue
    from collections import defaultdict
    cost = defaultdict(dict)
    frontier: PriorityQueue[tuple[int, Tile, Dir]] = PriorityQueue()
    for d in [1, 1j]:
        next_t = complex_to_tile(d)
        next_cost = int(M.tiles[next_t])
        cost[source][d] = 0
        frontier.put((0, source, next_t))

    while not frontier.empty():
        prio, curr_t, curr_d = frontier.get()
        curr_d = tile_to_complex(curr_d)

        for next_t, next_d, next_cost in branch(curr_t, curr_d):
            next_cost += cost[curr_t][curr_d]
            if next_d not in cost[next_t] or cost[next_t][next_d] > next_cost:
                cost[next_t][next_d] = next_cost
                priority = next_cost + heuristic(next_t, destination) * 5  # ignore type
                frontier.put((priority, next_t, complex_to_tile(next_d)))
    return cost


# part 1:
# rn = range(2, 5)
# part 2:
rn = range(5, 12)
p = findPath((0, 0), (M.maxX, M.maxY))
print(min(p[(M.maxX, M.maxY)].values()))
