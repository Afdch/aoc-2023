# instructions for Nitro
# import https://www.youtube.com/watch?v=hz_gOFRwTcg
# cheers
with open('d09.txt') as f:
    zs = [[int(x) for x in line.strip().split()] for line in f]


def find(seq: list[int], front=True):
    # print(f'\n{seq}')
    i = 0
    seqs = {i: seq}
    while True:
        i += 1
        seq = [seq[i+1] - seq[i] for i in range(len(seq)-1)]
        # print(seq)
        seqs[i] = seq
        # if sum(seq) == 0: # BAKAYARO
        if all([x == 0 for x in seq]):
            break
    if front:
        for j in range(i, 0, -1):
            seqs[j-1].append(seqs[j-1][-1] + seqs[j][-1])
            # print(seqs[j-1])
        # print(seqs[0][-1])
        return (seqs[0][-1])
    else:
        for j in range(i, 0, -1):
            seqs[j-1].insert(0, seqs[j-1][0] - seqs[j][0])
        return (seqs[0][0])


print(sum([find(seq) for seq in zs]))
print(sum([find(seq, False) for seq in zs]))
