from PIL import Image

colours = {
    '.': 1,
    '#': 3,
    'L': 0
    }

colourpalette = [
    16,      17,     45,    # #10112d
    236,    228,    207,    # #ece4cf
    214,     51,     32,    # #d63320
    49,      92,    147,    # #315c93
    125,    150,     58,    # #7d963a
    224,    146,     35,    # #e09223
    249,    222,     89,    # #f9de59
    249,    131,    101,    # #f98365
    195,     49,     36,    # #c33124
    161,    223,    251,    # #a1dffb
    78,     136,    138,    # #4e888a
    23,      41,     46,    # #17292e
    241,    150,     69,    # #f19645

]
colourpalette = colourpalette + [0]*(255 - len(colourpalette))


def printd(
    dic: dict,
    tileset: dict | None = None,
    highlight=[],
    reverse: bool = False,
    # sparse: False,
    max_x: int | None = None,
    max_y: int | None = None,
    offset_positives=True,
    emptytile: str = ' ',
    borders=False,
    title: str | None = None,
    _print=True
):
    """Prints a string representative of dots in the terminal
    Parameters
    ----------
    dic: dict
        dictionary with keys in a format of tuple (x, y) coodrdinates
        and integer value for tile

    tileset: dict
        a custom tileset

    reverse: bool
        render *y* coordinate reversed
    """

    X = []
    Y = []
    if dic:
        for tile in dic.keys():
            X.append(tile[0])
            Y.append(tile[1])
    else:
        X.append(0)
        Y.append(0)

    if tileset is None or isinstance(tileset, set):
        tileset = {x: str(x) for x in dic.values()}

    offset_x = min(X) if offset_positives else min(min(X), 0)
    offset_y = min(Y) if offset_positives else min(min(Y), 0)
    max_x = max_x or (max(X) - offset_x)
    max_y = max_y or (max(Y) - offset_y)

    a = [[emptytile for _ in range(max_x + 1)] for _ in range(max_y + 1)]
    if emptytile not in tileset:
        tileset[emptytile] = emptytile

    for tile in dic.keys():
        if dic[tile] is not None:
            a[tile[1] - offset_y][tile[0] - offset_x] = dic[tile]

    # print(a)
    lines: list[str] = []
    if borders and title:
        if len(title) > max_x:
            title = title[max_x - 2:]
        lines.append(f'╔{" \033[33;3;52m" + title + "\033[0m ":═^{max_x+17}}╗')
        pass
    elif borders:
        lines.append('╔' + '═'*(max_x + 3) + '╗')
    elif title:
        lines.append(f'\033[33;3;52m{title:^{max_x+1}}\033[0m;')
    for y, line in enumerate(a):
        nl = '║ ' if borders else ''
        for x, char in enumerate(line):
            c = tileset[char]
            if (x + offset_x, y + offset_y) in highlight:
                nl += "\033[94m" + c + "\033[0m"
            else:
                nl += c
        if borders:
            nl += ' ║'
        lines.append(nl)
    if borders:
        lines.append('╚' + '═'*(max_x + 3) + '╝')
    if reverse:
        lines[0], lines[-1] = lines[-1], lines[0]
        lines = list(reversed(lines))
    if _print:
        for line in lines:
            print(line)
    else:
        return lines


def side_by_side(ld: list[list[str] | None]):
    for ln in [' '.join(s) for s in zip(*ld)]:
        print(ln)


def imgd(dic: dict, save: bool = False, filename: str = 'test',
         scalefactor: int = 1, reverse: bool = True, colourscheme: dict = colours,
         colourpalette=colourpalette, size=(100, 100)):
    """makes an image out of the provided dictionary and saves it in the
    /output/ directory; default filename is "test"; or displays it depending
    on the mode
    Parameters
    ----------
    dic: dict
        dictionary with keys in a format of tuple (x, y) coodrdinates
        and integer value indicating the colour to print

    save: bool
        True: save image
        False: show image

    filename: str
        string indicating the filename

    scalefactor: int
        scale image by this factor

    reverse: bool
        render _y_ coordinate reversed

    colourscheme: dict
        dictionary with keys as integer and a RGB scheme in integer fromat
        (from 0 to 255)

    size: tuple
        uses user-defined size for the purpose of rendering image
    """

    image = mkimg(dic, scalefactor, reverse, colourscheme, colourpalette, size)

    # output?
    if save == 0:
        image.show()
    else:
        import os
        filepath = os.path.join(os.getcwd(), f"{filename}.png")
        image.save(filepath)


def mkimg(dic: dict, scalefactor: int = 1, reverse: bool = True,
          colourscheme: dict = colours, colourpalette=colourpalette, size=None):
    """Returns an image with out of the provided dictionary
    Parameters
    ----------
    dic: dict
        dictionary with keys in a format of tuple (x, y) coodrdinates
        and integer value indicating the colour to print

    save: bool
        True: save image
        False: show image

    filename: str
        string indicating the filename

    scalefactor: int
        scale image by this factor

    reverse: bool
        render _y_ coordinate reversed

    colourscheme: dict
        dictionary with keys as integer and a number from a colourpalette

    size: tuple
        uses user-defined size for the purpose of rendering image

    Return
    ------
    image: Image
    """
    # from PIL import Image
    X = []
    Y = []
    for tile in dic.keys():
        X.append(tile[0])
        Y.append(tile[1])

    min_x = min(X)
    max_x: int = max(X) - min(X) + 1
    min_y = min(Y)
    max_y: int = max(Y) - min(Y) + 1
    if size is None:
        size = (max_x, max_y)

    image = Image.new('P', size)
    image.putpalette(colourpalette)
    pixels = image.load()

    for x, y in dic.keys():
        YY = max_y - (y - min_y) - 1 if reverse else y - min_y
        pixels[x - min_x, YY] = colourscheme[dic[(x, y)]]
    # resize image?
    if scalefactor != 1:
        image = Image.Image.resize(image, (size[0]*scalefactor, size[1]*scalefactor), resample=Image.NEAREST)

    return image


def savegif(images: list[Image.Image], filename: str, duration: int | list, loop=0):
    import os
    if (type(duration) is list) and len(duration) != len(images):
        print(f'\033[93mLenght of duration \033[91m{len(duration)}\033[93m, \
              expected \033[94m{len(images)}\033[93m. \
              Reverting to default duration of \033[94m200 ms\033[93m.\033[0m')
        duration = 200
    filepath = os.path.join(os.getcwd(), f"{filename}.gif")
    images[0].save(filepath, save_all=True, append_images=images[1:],
                   duration=duration, fps=1, loop=loop)
