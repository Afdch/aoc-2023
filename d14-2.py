from aoc import Maze
from functools import cache
from time import time

with open('d14.txt') as f:
    M = Maze(f)


@cache
def cycle(flat: str):
    # north
    M = Maze(flat.split())
    for y in range(M.maxY + 1):
        for x in range(M.maxX + 1):
            if M.tiles[(x, y)] == 'O':
                y1 = y
                while y1 > 0 and M.tiles[(x, y1 - 1)] == '.':
                    M.tiles[(x, y1)] = '.'
                    y1 -= 1
                    M.tiles[(x, y1)] = 'O'
    # west
    for x in range(M.maxX + 1):
        for y in range(M.maxY + 1):
            if M.tiles[(x, y)] == 'O':
                x1 = x
                while x1 > 0 and M.tiles[(x1 - 1, y)] == '.':
                    M.tiles[(x1, y)] = '.'
                    x1 -= 1
                    M.tiles[(x1, y)] = 'O'
    # south
    for y in range(M.maxY, -1, -1):
        for x in range(M.maxX+1):
            if M.tiles[(x, y)] == 'O':
                y1 = y
                while y1 < M.maxY and M.tiles[(x, y1 + 1)] == '.':
                    M.tiles[(x, y1)] = '.'
                    y1 += 1
                    M.tiles[(x, y1)] = 'O'
    # east
    for x in range(M.maxX, -1, -1):
        for y in range(M.maxY + 1):
            if M.tiles[(x, y)] == 'O':
                x1 = x
                while x1 < M.maxX and M.tiles[(x1 + 1, y)] == '.':
                    M.tiles[(x1, y)] = '.'
                    x1 += 1
                    M.tiles[(x1, y)] = 'O'
    return M.flatten()


def calc_load(M):
    load = 0
    for rock in M.tiles:
        if M.tiles[rock] == 'O':
            ld = M.maxY - rock[1] + 1
            load += ld
    return load


t = time()
flat = M.flatten()
for i in range(1000000000):
    flat = cycle(flat)
    if i % 100000 == 0:
        print(f'{i/10000000:>7.2f}% | {time() - t:>6.3f}s', end='\r')
print('\n', calc_load(Maze(flat.split())))
