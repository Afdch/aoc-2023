from aoc import Maze
from queue import Queue
Tile = tuple[int, int]


with open('d10.txt') as f:
    M = Maze(f)


start = M.features['S'][0]
pipes = ["-", '|', 'J', 'F', 'L', '7']
print(start)
Q = Queue()

for n in [(1, 0), (-1, 0), (0, -1), (0, 1)]:
    x, y = n
    nei = start[0] + x, start[1] + y
    if nei[0] < 0 or nei[0] > M.maxX or nei[1] < 0 or nei[1] > M.maxY:
        continue
    test = M.tiles[nei]
    if (((x == 1) and test not in ['-', 'J', '7']) or
        ((x == -1) and test not in ['-', 'F', 'L']) or
        ((y == 1) and test not in ['|', 'J', 'L']) or
        ((y == -1) and test not in ['|', 'F', '7'])):
        continue
    Q.put((nei, 1))


explored = [start]
dists = {start[0]: 0}


def get_neighbours(tile: Tile):
    x, y = tile
    nei: list[Tile] = []
    form = M.tiles[tile]
    if form == '-':
        neighbours = [(x + 1, y), (x - 1, y)]
    elif form == '|':
        neighbours = [(x, y + 1), (x, y - 1)]
    elif form == 'J':
        neighbours = [(x, y - 1), (x - 1, y)]
    elif form == 'F':
        neighbours = [(x + 1, y), (x, y + 1)]
    elif form == 'L':
        neighbours = [(x, y - 1), (x + 1, y)]
    elif form == '7':
        neighbours = [(x - 1, y), (x, y + 1)]
    else:
        neighbours = []
    for x1, y1 in neighbours:
        if 0 <= x1 <= M.maxX and 0 <= y1 <= M.maxY:
            if (x1, y1) not in explored:
                nei.append((x1, y1))
    return nei


def part1():
    while Q.qsize() > 0:
        current, dist = Q.get()
        dists[current] = dist
        explored.append(current)
        for nei in get_neighbours(current):
            if nei in explored:
                continue
            if M.tiles[nei] not in pipes:
                continue
            Q.put((nei, dist + 1))
    # print(dists.values())


part1()
print(max(dists.values()))


unexplored = [x for x in M.tiles if x not in explored]
# from printd import printd
# replacement = {
#     '-': '─',
#     '|': '│',
#     '7': '┐',
#     'J': '┘',
#     'F': '┌',
#     'L': '└',
#     'S': '*',
#     '.': ' '
# }
# printd({tile: replacement[M.tiles[tile]] for tile in M.tiles}, reverse=False)
# printd({tile: '#'if tile in unexplored else ' ' for tile in M.tiles}, reverse=False)

# manual repacement
# M.tiles[(8, 42)] = 'F'
# TODO replace with general case solution
nnn = 0
for n in [(1, 0), (-1, 0), (0, -1), (0, 1)]:
    x, y = n
    nei = start[0] + x, start[1] + y
    if nei[0] < 0 or nei[0] > M.maxX or nei[1] < 0 or nei[1] > M.maxY:
        continue
    if x > 1 and M.tiles[nei] in ['7', '-', 'J']: nnn += 1
    if x < 1 and M.tiles[nei] in ['L', '-', 'F']: nnn += 100
    
    if y < 1 and M.tiles[nei] in ['L', '|', 'J']: nnn += 10
    if y > 1 and M.tiles[nei] in ['7', '|', 'F']: nnn += 1000
repl = {
      11: "F",
     101: "-",
    1001: "L",
     110: "7",
    1010: "|",
    1100: "J" 
}
M.tiles[start] = repl[nnn]

poob = 0
for x, y in unexplored:
    xn = x
    boop = 0
    while xn < M.maxX:
        xn += 1
        if M.tiles[(xn, y)] in ['|', 'F', '7'] and (xn, y) not in unexplored:
            boop += 1
    if boop % 2 != 0:
        # print(x, y)
        poob += 1
print(poob)
