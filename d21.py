from aoc import Maze, Tile
from itertools import chain

with open('d21.txt') as f:
    M = Maze(f, mode='maze')

# steps = []
print(M.objects)


def neighbors(t: Tile) -> list[tuple[Tile, complex]]:
    x, y = t
    nei: list[tuple[Tile, complex]] = []

    neighbours = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]
    for x1, y1 in neighbours:
        if (x1, y1) not in M.walls:
            if x1 < 0:
                nei.append(((M.maxX, y1), -1))
            elif x1 > M.maxX:
                nei.append(((0, y1), 1))
            elif y1 < 0:
                nei.append(((x1, M.maxY), -1j))
            elif y1 > M.maxY:
                nei.append(((x1, 0), 1j))
            else:
                nei.append(((x1, y1), 0))

            # if 0 <= x1 <= M.maxX and 0 <= y1 <= M.maxY:
            #     nei.append(((x1, y1), 0))
            # else:
            #     nei.append(((x1 % M.maxX, y1 % M.maxY), 1))

    return nei


neis = {t: neighbors(t) for t in M.tiles if t not in M.walls}


def take_step(steps):
    return set(chain(*[neis[t] for t in steps]))


# part 2
def traverse_chunk(seed, steps_n):
    curr = set(((seed, 0),))
    curr_l = 1
    prev = set()
    prev_l = 0
    for i in range(steps_n):
        all_next = set((step, pl) for step, pl in chain(*[take_step(tuple(x for x, _ in curr))]) if pl == 0)
        nxt = all_next - prev
        nxt_l = prev_l + len(nxt)
        prev = curr
        prev_l = curr_l
        curr = nxt
        curr_l = nxt_l
    return curr_l


seeds_cart = [
    (0, 65),
    (65, 0),
    (130, 65),
    (65, 130),
]

seeds_diag = [
    (0, 0),
    (0, 130),
    (130, 130),
    (130, 0),
]

# less by 1 because starting seed is the first step
cart = [traverse_chunk(s, 130) for s in seeds_cart]
diag_short = [traverse_chunk(s, 64) for s in seeds_diag]
diag_long = [traverse_chunk(s, 131 + 64) for s in seeds_diag]
print(cart, sum(cart))
print(diag_short, sum(diag_short))
print(diag_long, sum(diag_long))

s = set([traverse_chunk(s, 2*131 + 65) for s in seeds_cart])
if len(s) != 1:
    raise Exception
s_even = list(s)[0]

s = set([traverse_chunk(s, 131 + 65) for s in seeds_cart])
if len(s) != 1:
    raise Exception
s_odd = list(s)[0]
print(s_even, s_odd)


def calculate_all_chunks(n):
    k_e = int(n/2)
    k_o = int((n + 1)/2)

    even_chunks = 4*k_e**2
    odd_chunks = 4*k_o*(k_o - 1) + 1

    return (even_chunks, odd_chunks, even_chunks + odd_chunks,
            (even_chunks*s_even +
             odd_chunks*s_odd +
             n*sum(diag_short) +
             (n - 1)*sum(diag_long) +
             sum(cart)))


# [print(i, whoosh(i)) for i in range(10)]

print(calculate_all_chunks(202300)[3])
