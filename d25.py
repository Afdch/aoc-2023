import networkx as nx
# import matplotlib.pyplot as plt

G = nx.Graph()

with open('d25.txt') as f:
    for line in f:
        a, bs = line.strip().split(': ')
        for b in bs.split(' '):
            G.add_edge(a, b)

# pos = nx.kamada_kawai_layout(G)
# nx.draw_kamada_kawai(G, with_labels=True)
# plt.show()
# eyeball node names
G.remove_edge('tmt', 'pnz')
G.remove_edge('gbc', 'hxr')
G.remove_edge('mvv', 'xkz')
sg = list(nx.connected_components(G))
print(len(sg[0]) * len(sg[1]))
