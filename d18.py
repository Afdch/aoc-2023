directory = {
    'U': -1j,
    'D': +1j,
    'L': -1,
    'R': +1
}
hex_to_direction = {
    '0': 'R',
    '1': 'D',
    '2': 'L',
    '3': 'U',
}

vertices = []
pointer = 0
directions = []
lengths = []
with open('d18.txt') as f:
    for i, line in enumerate(f):
        direction, length, colour = line.strip().split()

        # part 2 -- NB! comment out for p1 solution
        direction = hex_to_direction[colour[-2]]
        length = int(colour[2:-2], 16)

        directions.append(direction)
        lengths.append(int(length))

dirs = {
    'R': {'D': 1, 'U': -1},
    'D': {'L': 1, 'R': -1},
    'L': {'U': 1, 'D': -1},
    'U': {'R': 1, 'L': -1},
}
addition = 0
vertices.append((0, 0))
for i in range(len(directions)-1):
    angle_before = dirs[directions[i]][directions[i + 1]]
    angle_after = dirs[directions[i - 1]][directions[i]]
    addition = angle_before if angle_before == angle_after else 0

    pointer += directory[directions[i]] * (int(lengths[i]) + addition)
    vertices.append((pointer.real, pointer.imag))

S = 0
for n in range(len(vertices)):
    S += (vertices[n-1][0])*(vertices[n][1]) - (vertices[n][0])*(vertices[n-1][1])
print(int(abs(S)/2))
