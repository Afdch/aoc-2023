from collections import defaultdict
from printd import printd, side_by_side
from itertools import chain, product
from aoc import print_percent
blocks = []
tiles = {}

with open('d22.txt') as f:
    for i, line in enumerate(f):
        fr, to = [[int(y) for y in x.split(',')] for x in line.strip().split('~')]

        z_min, z_max = sorted([fr[2], to[2]])
        y_min, y_max = sorted([fr[1], to[1]])
        x_min, x_max = sorted([fr[0], to[0]])
        block = []
        for z in range(z_min, z_max + 1):
            for y in range(y_min, y_max + 1):
                for x in range(x_min, x_max + 1):
                    block.append((x, y, z))
                    tiles[(x, y, z)] = i
        blocks.append(block)

print(f'Reading done, total blocks: {len(blocks)}\n')


# region fancypringing
def get_slice(tls, axis, c=None):
    match axis:
        case 'y':
            return {(x, z): chr(65 + tls[(x, y, z)] % 62) for x, y, z in tls if (c is None) or (y == c)}
        case 'x':
            return {(y, z): chr(65 + tls[(x, y, z)] % 62) for x, y, z in tls if (c is None) or (x == c)}
        case 'z':
            return {(x, y): chr(65 + tls[(x, y, z)] % 62) for x, y, z in tls if (c is None) or (z == c)}
        case _:
            raise Exception
# endregion


t = {c: i for c, i in chain(*[product(c, [i]) for i, c in enumerate(blocks)])}
printd(get_slice(t, 'y'), reverse=True, emptytile='.', borders=True, title='y')
fallen = []
treeUP = defaultdict(list)
treeDW = defaultdict(list)
Z = sorted(range(len(blocks)), key=lambda k: min(list(zip(*blocks[k]))[2]))

for j, i in enumerate(Z):
    b = blocks[i]
    # print(b)
    shift_z = 0
    while True:
        if any(z - shift_z <= 1 for x, y, z in b):
            # print(f'{b} hit the ground\n')
            break
        if any((x, y, z - shift_z - 1) in fallen for x, y, z in b):
            # detect which block was touched
            for x, y, z in b:
                if (x, y, z - shift_z) in tiles:
                    treeDW[i].append(tiles[(x, y, z - shift_z)])
                    treeUP[tiles[(x, y, z - shift_z)]].append(i)
            break
        shift_z += 1
    if shift_z > 0:
        new_block = []
        for block in blocks[i]:
            del tiles[block]
        for x, y, z in b:
            shift_b = x, y, z - shift_z
            new_block.append(shift_b)
            tiles[shift_b] = i
        blocks[i] = new_block
    fallen.extend(blocks[i])
    print_percent(j, len(Z), 'Generating a relationship tree: ', value=j+1)
    pass  # TODO
# print(tiles)
print('Tree complete\n')
# print(treeUP)
# print(treeDW)
t = {c: i for c, i in chain(*[product(c, [i]) for i, c in enumerate(blocks)])}
# printd(get_slice(t, 'y'), reverse=True, emptytile='.', borders=True, title='y')
# printd(get_slice(t, 'x'), reverse=True, emptytile='.', borders=True, title='x')
max_x = 10
max_y = max(z for x, y, z in t)

side_by_side([printd(get_slice(t, 'x', i),
                     reverse=True,
                     emptytile='.',
                     borders=True,
                     max_x=max_x-1,
                     max_y=max_y,
                     title=f'x={i}',
                     offset_positives=False,
                     _print=False) for i in range(max_x)])

side_by_side([printd(get_slice(t, 'y', i),
                     reverse=True,
                     emptytile='.',
                     borders=True,
                     max_x=max_x-1,
                     max_y=max_y,
                     title=f'y={i}',
                     offset_positives=False,
                     _print=False) for i in range(max_x)])

disitegrate = []
for j, t in enumerate(Z):
    if t not in treeUP or all(len(treeDW[x]) > 1 for x in treeUP[t]):
        disitegrate.append(t)
    print_percent(j, len(Z), 'Disintegrating: ', value=len(disitegrate))
# print([chr(65 + t) for t in disitegrate])
print(f'Blocks can be disintegrated: {len(disitegrate)}')

# 645 is too high
