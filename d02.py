games = {}
with open('d02.txt') as f:
    lines = [line.strip() for line in f.readlines()]


# part 1
for line in lines:
    game, cubes = line.strip().split(':')
    game = int(game[4:])
    # hardcoding colours, hoping imput wouldn't have more of them
    # spoiler: it didn't
    games[game] = {'red': [], 'green': [], 'blue': []}
    # print(game)
    pulls = cubes.strip().split(';')
    for pull in pulls:
        colours = pull.strip().split(',')
        for colour in colours:
            # print(colour)
            amount, col = colour.split()
            games[game][col].append(int(amount))
# print(games)

max_cubes = {'red': 12, 'green': 13, 'blue': 14}
impossible_ids = []

for game in games:
    for cube_type in max_cubes:
        if any([x > max_cubes[cube_type] for x in games[game][cube_type]]):
            # print(games[game][cube_type])
            impossible_ids.append(game)

# print(impossible_ids)
print(sum([x for x in games if x not in impossible_ids]))


# part 2
sum_power = 0
for game in games:
    # because can't multiplication reduction without imports
    power = max(games[game]['red'])*max(games[game]['green'])*max(games[game]['blue'])
    print(power)
    sum_power += power
print(sum_power)

# bonus one-liner
# print(sum([max(games[game]['red'])*max(games[game]['green'])*max(games[game]['blue']) for game in games]))

# bonus three-liner
# from operator import mul
# from functools import reduce
# print(sum([reduce(mul, [max(games[game][c]) for c in max_cubes], 1) for game in games]))
