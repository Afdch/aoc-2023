# abusing last year's codebase because why not amirite
from aoc import Maze

with open('d03.txt') as f:
    field = Maze(f, mode="field")


# print(tiles.objects)

def get_number(tile) -> int:
    x, y = tile
    while field.tiles[(x, y)].isdigit():
        x -= 1
        if x < 0:
            break
    num = []
    x += 1
    while field.tiles[(x, y)].isdigit():
        num.append((x, y))
        x += 1
        if x > field.maxX:
            break
    if len(num) > 0:
        return int(''.join(field.tiles[x] for x in num))
    else:
        return 0


# part 1:
# aka you don't want to check my first implementation
summ = 0  # why had I used a system function name? who knows
things = [x for x in field.objects if not field.tiles[x].isdigit()]
for thing in things:
    # print(thing, tiles.tiles[thing])
    digits = [x for x in field.get_neighbours(thing, mode='full') if field.tiles[x].isdigit()]
    # assuming there are no identical part numbers, please, please, please
    for digit in list(set([get_number(x) for x in digits])):
        summ += digit
print(summ)
# bonus one-liner (not really)
summ = sum([sum(list(set([get_number(x) for x in [x for x in field.get_neighbours(thing, mode='full')
            if field.tiles[x].isdigit()]]))) for thing in [x for x in field.objects if not field.tiles[x].isdigit()]])

# part 2
# aka implying this implementation is any better smh fr fr
gears = [x for x in field.objects if field.tiles[x] == '*']
gears_sum = 0
for gear in gears:
    dis = []
    digits = [x for x in field.get_neighbours(gear, mode='full') if field.tiles[x].isdigit()]
    # print(digits)
    ys = set(y for _, y in digits)
    xs = list(set(x for x, _ in digits))
    # if there are two different ys => guaranteed to have 2 different numbers
    # if only 1 y, but 2 x which are exactly 2 apart, this means gear is between 2 numbers as well
    if len(ys) == 2 or (len(ys) == 1) and (len(xs) == 2) and (abs(xs[1] - xs[0]) == 2):
        dis = list(set([get_number(x) for x in digits]))
        gears_sum += dis[0]*dis[1]
    # print (gear, dis)

print(gears_sum)
