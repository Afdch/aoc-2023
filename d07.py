from collections import Counter
from functools import cmp_to_key
with open('d07.txt') as f:
    lines = [line.strip().split() for line in f.readlines()]


def compare_cards(c1: str, c2: str) -> int:
    for i in range(5):
        if c1[i] < c2[i]:
            return -1
        if c1[i] > c2[i]:
            return 1
    raise Exception('There are 2 identical hands in the input')


def sorting(card1: tuple[str, int], card2: tuple[str, int]) -> int:
    c1 = Counter(card1[0])
    j1 = c1['1']
    del c1['1']
    c1 = c1.most_common(5)

    c2 = Counter(card2[0])
    j2 = c2['1']
    del c2['1']
    c2 = c2.most_common(5)

    # check if there are all 5 jokers
    # assume no 2 all joker hands exist
    if j1 == 5:
        if c2[0][1]+j2 < 5:
            return 1
        return compare_cards(card1[0], card2[0])
    if j2 == 5:
        if c1[0][1]+j1 < 5:
            return -1
        return compare_cards(card1[0], card2[0])

    # check if there's an obvious larger group.
    if c1[0][1]+j1 < c2[0][1]+j2:
        return -1
    if c1[0][1]+j1 > c2[0][1]+j2:
        return 1
    # 5oak, 4oak
    if c1[0][1]+j1 in (4, 5):
        return compare_cards(card1[0], card2[0])

    # first largest group is equal, need to check if there's 2nd larges group
    # left: 32, 3, 22, 2, 1
    if (c1[0][1]+j1 == 3 and c2[0][1]+j2 == 3) or (c1[0][1]+j1 == 2 and c2[0][1]+j2 == 2):
        # 33 vs 3
        # OR
        # 22 vs 2
        if c1[1][1] < c2[1][1]:
            return -1
        if c1[1][1] > c2[1][1]:
            return 1
    # left 1 OR latest card in a pile

    # checking 2nd highest card
    return compare_cards(card1[0], card2[0])


p: list[tuple[str, int]] = []
# part 1
for line in lines:
    card = line[0].replace('A', 'F').replace('K', 'D').replace('Q', 'C').replace('J', 'B').replace('T', 'A')
    bid = int(line[1])
    p.append((card, bid))

winnings = 0
for i, g in enumerate(sorted(p, key=cmp_to_key(sorting))):
    # print(i+1, Counter(g[0]) ,g[1])
    winnings += (i+1)*g[1]
print('part 1:', winnings)


# part 2
p = [(x.replace('B', '1'), y) for (x, y) in p]

winnings = 0
for i, g in enumerate(sorted(p, key=cmp_to_key(sorting))):
    # print(i+1, g[0] ,g[1])
    winnings += (i+1)*g[1]
print('part 2:', winnings)
