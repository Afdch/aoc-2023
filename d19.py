from functools import reduce
from operator import mul

with open('d19.txt') as f:
    workflows, ratings = f.read().split('\n\n')


# did this part just for fun, there's literally no need to
for yeet in [':A,A}', ':R,R}']:
    while yeet in workflows:
        index = workflows.find(yeet)
        index2 = index
        # backtrack until we find a comma or a {
        while index > 0 and workflows[index] not in [',', '{']:
            index -= 1
        if workflows[index] == ',':
            workflows = workflows[:index + 1] + workflows[index2 + 1:]
        elif workflows[index] == '{':
            index3 = index
            # the whole rule is useless, need to replace it in every instance and yeet
            while index > 0 and workflows[index] != '\n':
                index -= 1

            useless_trash = workflows[index+1: index3]
            while index2 < len(workflows) and workflows[index2] != '\n':
                index2 += 1

            workflows = workflows[:index] + workflows[index2:]
            workflows = workflows.replace(':' + useless_trash + ',', ':' + yeet[1] + ',')
            workflows = workflows.replace(',' + useless_trash + '}', ',' + yeet[1] + '}')
            pass
        workflows = workflows.replace(',A,A}', ',A}')
        workflows = workflows.replace(',R,R}', ',R}')

strRules = {}
rulesets = {}
# print(len(workflows))
for w in workflows.split('\n'):
    name, rules = w[:-1].split('{')
    # print(rules)
    strRules[name] = rules

for name in strRules:
    rulesets[name] = strRules[name].split(',')


rulesets['A'] = 'A'
rulesets['R'] = 'R'


def checkPart(r):
    v = {}
    for var in r[1:-1].split(','):
        key, value = var.split('=')
        v[key] = int(value)
    workflow = 'in'
    while True:
        for rule in rulesets[workflow]:
            if rule == 'A':
                # accepted:
                ret = sum(v[k] for k in 'xmas')  # v['x']+v['m']+v['a']+v['s']
                return ret
            if rule == 'R':
                return 0
            if ':' not in rule:
                workflow = rule
                break
            condition, jump = rule.split(':')
            if '<' in condition:
                var, number = condition.split('<')
                if v[var] < int(number):
                    workflow = jump
                    break
            else:
                var, number = condition.split('>')
                if v[var] > int(number):
                    workflow = jump
                    break


# part 1
answer = 0
for r in ratings.split('\n'):
    a = checkPart(r)
    # print(a)
    answer += a
print(answer)


# part 2
def quad_volume(d: dict, command):
    volume = 0
    rules = rulesets[command]
    for rule in rules:
        if ':' in rule:
            ev, ind = rule.split(':')
            var = ev[0]
            num = int(ev[2:])
            d_n = {key: val for key, val in d.items()}
            if ev[1] == '<':
                d_n[var] = (d_n[var][0], num - 1)
                d[var] = (num, d[var][1])
            else:
                d[var] = (d[var][0], num)
                d_n[var] = (num + 1, d_n[var][1])
            if ind == 'A':
                volume += reduce(mul, (d_n[k][1] - d_n[k][0] + 1 for k in d_n))
            elif ind == 'R':
                continue
            else:
                volume += quad_volume(d_n, ind)
        elif rule == 'A':
            return volume + reduce(mul, (d[k][1] - d[k][0] + 1 for k in d))  # + what??
        elif rule == 'R':
            return volume
        else:
            volume += quad_volume(d, rule)
    return volume


print(quad_volume({k: (1, 4000) for k in 'xmas'}, 'in'))
