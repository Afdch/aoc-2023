with open('d04.txt') as f:
    lines = [line.strip() for line in f.readlines()]

cards = []
copies = [1 for _ in lines]
poke = []

for card, line in enumerate(lines):
    winning, have = line[line.find(':')+1:].split(' | ')
    winning = {int(x) for x in winning.split()}
    have = {int(x) for x in have.split()}
    # part 1
    count = len(winning & have)
    poke.append(count)
    cards.append(int(2**(count-1)))
    # part 2
    for copy in range(count):
        copies[card+copy+1] += copies[card]

# part 1
print(sum(cards))

# part 2
print(sum(copies))


totsum = 0
X = [1 for _ in lines]
for n in range(len(lines)):
    subsum = 0
    for k in range(n):
        if k - n <= poke[n]:
            if poke[n] > 0:
                subsum += X[k]
    print(subsum)
    totsum += subsum
    X[n] += subsum
print(totsum)
print(sum(X))
